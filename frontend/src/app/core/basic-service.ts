import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

import { map } from 'rxjs/operators';

export class BasicService {

   constructor(
       public http: HttpClient
       ) {

   }
   getAll(): Observable<any> {
      return this.http.get<any>(environment.api);
   }

   getById(id: string): Observable<any> {
      return this.http.get<any>('');
   }

   getByName(name: string, queryParams = ''): Observable<any> {
      return this.http.get<any>('');
   }

   getByAttr(attr: string, text: string, queryParams = ''): Observable<any> {
      return this.http.get<any>('');
   }

   create(data: any): Observable<any> {
      delete data._id;
      return this.http.post('', data);
   }

   add(data: any, queryParams = ''): Observable<any> {
      return this.http.post('', data);
   }

   remove(id: string, queryParams = ''): Observable<any> {
      return this.http.delete('');
   }

   update(_id: string, data: any, queryParams = ''): Observable<any> {
      const body = Object.assign({}, data);
      delete body._id;
      delete body.company;

      return this.http
         .put('', body)
         .pipe(map((res: any) => Object.assign({ _id }, res)));
   }

   push(id: string, key: string, data: any, queryParams = ''): Observable<any> {
      return this.http.post('', data);
   }

   pull(...params:any): Observable<any> {
      return this.http.delete('');
   }
}