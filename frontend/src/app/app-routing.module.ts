import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { SesionloginComponent } from './pages/sesionlogin/sesionlogin.component';
import { SesionregisterComponent } from './pages/sesionregister/sesionregister.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./pages/home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'favoritos',
        loadChildren: () =>
          import('./pages/favoritos/favoritos.module').then(
            (m) => m.FavoritosModule
          ),
      },
      {
        path: 'acerca-de',
        loadChildren: () =>
          import('./pages/acerca-de/acerca-de.module').then(
            (m) => m.AcercaDeModule
          ),
      },
      {
        path: 'nosotros',
        loadChildren: () =>
          import('./pages/nosotros/nosotros.module').then(
            (m) => m.NosotrosModule
          ),
      },
      {
        path: 'categorias',
        loadChildren: () =>
          import('./pages/categorias/categorias.module').then(
            (m) => m.CategoriasModule
          ),
      },
    ],
  },
  {
    path: 'sesionLogin',
    component: SesionloginComponent,
  },
  {
    path: 'sesionRegister',
    component: SesionregisterComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
