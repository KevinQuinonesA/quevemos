import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { MoviesService } from '@app/services/movies.service';
import { FavoritosService } from '@app/services/favoritos.service';

import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss'],
})
export class SearcherComponent implements OnInit {
  @ViewChild('container') movieContainer!: ElementRef;
  faCoffee = faCoffee;
  pelicula: any[] = [];
  title = new FormControl('');
  image_base_path: string = 'https://image.tmdb.org/t/p/w185';

  constructor(
    private moviesService: MoviesService,
    private favoritosService: FavoritosService
  ) {}

  ngOnInit(): void {}

  getRecommendation() {
    this.pelicula=[];
    this.moviesService
      .getRecommendation(this.title.value)
      .subscribe((res: any) => {
        // this.pelicula = res;
        for (var key in res){
          this.pelicula.push(res[key]);
        }   
      });
  }

  showModal() {
    Swal.fire({
      icon: 'success',
      title: '¡Se agrego a Favoritos!',
      text: 'Esta pelicula fue enviada a la seccion de favoritos',
      footer:
        '<a href="/favoritos">¿Desea ingresar a la seccion de Favoritos?</a>',
    });
  }

  enviarcart(cart: any) {
    this.favoritosService.enviarFavoritos(cart.id).subscribe((data) => {
      console.log(data)
    });
  }

  // quitarFavoritos(cart: any) {
  //   this.favoritosService.quitarFavoritos(cart.id).subscribe((data) => {
  //     console.log(data)
  //   });
  // }
}
