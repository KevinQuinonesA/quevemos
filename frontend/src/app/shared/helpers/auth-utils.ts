export function getToken(): string {
    var token = localStorage.getItem('token');
    if (token == null || token == undefined) {
        return '';
    }
    return token;
}

export function setToken(token: string) : void {
    localStorage.setItem('token',token);
}