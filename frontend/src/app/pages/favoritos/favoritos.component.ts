import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FavoritosService } from '@app/services/favoritos.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss'],
})
export class FavoritosComponent implements OnInit {
  constructor(
    private favoritosService: FavoritosService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  image_base_path: string = 'https://image.tmdb.org/t/p/w185';
  cartrecibido: string = '';
  pelicula: any[] = [];

  ngOnInit(): void {
    //this.recibircart()
    this.recibircart();
  }

  //recibircart(){
  //  console.log("entro a la funcion recibir")
  //this.favoritosservice.myFavorites.subscribe(data=>{
  //this.cartrecibido=data
  //console.log('ESTO RECIBO',this.cartrecibido)
  // })

  //}

  recibircart() {
    this.favoritosService.obtenerFavoritos().subscribe((res) => {
      this.pelicula = res.data;
      console.log(this.pelicula);
    });
  }

  quitarFavoritos(cart: any) {
    this.favoritosService.quitarFavoritos(cart.id).subscribe((res) => {
      console.log(res)
      document.location.reload()
    });
  }
}
