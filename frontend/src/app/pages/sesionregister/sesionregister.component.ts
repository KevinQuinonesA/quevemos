import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { RegisterService } from 'app/services/register.service';
import { Register } from 'app/models/Register';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-sesionregister',
  templateUrl: './sesionregister.component.html',
  styleUrls: ['./sesionregister.component.scss']
})
export class SesionregisterComponent implements OnInit {

  registerForm=new FormGroup({
    name: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
  })
  registerError=false;

  constructor(private registerService:RegisterService,private router:Router) { }

  ngOnInit(): void {
  }

  onRegister(form:Register){
    this.registerService.authRegister(form).subscribe(data =>{
      console.log(data)
      console.log("Ingreso")
      this.router.navigate(['/'])
    },error=>{
      this.registerError=true;
      this.router.navigate(['/sesionRegister'])});
  }

  name: any='';
  user: any='';
  pass: any='';

  clickEvent(){
    this.name=this.registerForm.get('name')?.value;
    this.user=this.registerForm.get('email')?.value;
    this.pass=this.registerForm.get('password')?.value;
    this.onRegister(this.registerForm.value);
  }
  RegisterFailed(){
    return this.registerError;
  }

  showModal(){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Se Registro de manera exitosa!',
      showConfirmButton: false,
      timer: 1000
    })
  }

}
