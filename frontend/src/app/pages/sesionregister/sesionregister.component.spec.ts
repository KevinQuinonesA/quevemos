import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SesionregisterComponent } from './sesionregister.component';

describe('SesionregisterComponent', () => {
  let component: SesionregisterComponent;
  let fixture: ComponentFixture<SesionregisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SesionregisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SesionregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
