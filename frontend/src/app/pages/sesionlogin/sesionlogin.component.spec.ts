import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SesionloginComponent } from './sesionlogin.component';

describe('SesionloginComponent', () => {
  let component: SesionloginComponent;
  let fixture: ComponentFixture<SesionloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SesionloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SesionloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
