import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { LoginService } from 'app/services/login.service';
import { Login } from 'app/models/Login';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-sesionlogin',
  templateUrl: './sesionlogin.component.html',
  styleUrls: ['./sesionlogin.component.scss']
})
export class SesionloginComponent implements OnInit {

  
  loginForm=new FormGroup({
    email : new FormControl('',Validators.required),
    password : new FormControl('',Validators.required),
  })
  loginError=false;
  constructor(private loginService:LoginService,private router:Router) { }

  ngOnInit(): void {
 
  }
  onLogin(form:Login){
    this.loginService.authLogin(form).subscribe(data =>{
      if(data.token!=null){
        console.log(data)
        console.log("INGRESO")
        localStorage.setItem('token',data.token);
        //localStorage.setItem('usuario',data.usuario);
        this.router.navigate(['/'])
      }
    },error=>{
      this.loginError=true;
      this.router.navigate(['/sesionLogin'])});  
  }
  flag:boolean=false;
  user: any = '';
  pass: any = '';

  clickEvent(){
    this.user=this.loginForm.get('email')?.value;
    this.pass=this.loginForm.get('password')?.value;
    this.onLogin(this.loginForm.value);
    }

  LoginFailed(){
    return this.loginError;
  }

  showModal(){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Se ingreso de manera correcta',
      showConfirmButton: false,
      timer: 1000
    })
  }

}
