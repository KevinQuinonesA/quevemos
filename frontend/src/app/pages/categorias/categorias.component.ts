import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '@app/services/movies.service';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss'],
})
export class CategoriasComponent implements OnInit {
  @ViewChild('container') movieContainer!: ElementRef;
  faCoffee = faCoffee;
  pelicula: any[] = [];
  title = new FormControl('');
  image_base_path: string = 'https://image.tmdb.org/t/p/w185';

  constructor(
    private moviesService: MoviesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.categoria) {
        this.getByCategory(params.categoria);
      }
    });
  }

  getByCategory(category: string) {
    this.moviesService.getByCategory(category).subscribe((res: any) => {
      this.pelicula = res.result;
    });
  }
}
