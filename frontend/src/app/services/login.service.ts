import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Login } from '../models/Login';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  url:string="https://qvemosbk.herokuapp.com/login/";


  constructor(private http:HttpClient) { }

  authLogin(form:Login):Observable<any>{
    return this.http.post(this.url, form);
  }
}
