import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Favoritos } from '../models/Favoritos';
import { Observable } from 'rxjs';
import { getToken } from '../shared/helpers/auth-utils';
import { Injectable, Output, EventEmitter } from '@angular/core';
@Injectable({
    providedIn: 'root'
  })
  export class FavoritosService {

    private headers = new HttpHeaders();

    url:string="https://qvemosbk.herokuapp.com/favorites";
   // @Output() myFavorites: EventEmitter<string>=new EventEmitter();
  
  
    constructor(private http:HttpClient) {
      this.authorizationHeader();
    }
    
    authorizationHeader(): void {
      const token = getToken();
      if (token != '')
          this.headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    }

    enviarFavoritos(idmovie:number):Observable<any>{
      this.authorizationHeader();
      return this.http.post(this.url , {movie_id: idmovie} ,  { headers: this.headers });
    }
    
    obtenerFavoritos():Observable<any>{
      this.authorizationHeader();
      return this.http.get(this.url,{ headers:this.headers });
    }

    quitarFavoritos(idmovie:number):Observable<any>{
      this.authorizationHeader();
      return this.http.delete(this.url + "/" + idmovie ,  { headers: this.headers });
    }
  }
  