import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  constructor(private http: HttpClient) {}
  image_base_path: string = 'https://image.tmdb.org/t/p/w185';
  url_recommendation = '/recomend';
  url_categories = '/movies/genres/';

  private headers = new HttpHeaders();

  peliculaLista: any[] = [
    {
      titulo: 'WALL·E',
      descripcion:
        'Tras cientos de años haciendo aquello para lo que fue construido: limpiar el planeta de basura, el pequeño robot Wall-e tiene una nueva misión cuando conoce a Eva.',
      fecha: '24/07/2008',
      calificacion: '8.4',
      imagen:
        'https://www.encadenados.org/rdc/images/stories/rashomon/num_91/walle-1.jpg',
    },
    {
      titulo: 'Toy Story',
      descripcion:
        "Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear",
      fecha: '1995-10-30',
      calificacion: '9',
      imagen: 'http://image.tmdb.org/t/p/w185//rhIRbceoE9lR4veEXuwCC2wARtG.jpg',
    },
    {
      titulo: 'Jumanji',
      descripcion:
        'When siblings Judy and Peter discover an enchanted board game that opens the door to a magical world',
      fecha: '1995-12-15',
      calificacion: '8',
      imagen: this.image_base_path + '/vzmL6fP7aPKNKPRTFnZmiUfciyV.jpg',
    },
    {
      titulo: 'Sudden Death',
      descripcion:
        'International action superstar Jean Claude Van Damme teams with Powers Boothe in a Tension-packed',
      fecha: '1995-12-22',
      calificacion: '7',
      imagen: 'https://m.media-amazon.com/images/I/514ekbt5jzL._SY445_.jpg',
    },
  ];

  getpelicula() {
    return this.peliculaLista;
  }

  /**
   * Operation that queries the API by GET method, in general view a record
   *
   * @method getRecommendation
   * @param url It is the query URL
   * @return Any record or records in the database wrapped asynchronously
   */
  getRecommendation<T>(name: string): Observable<T> {
    return this.http.post<T>(environment.api + this.url_recommendation, {movie_title: name} );
  }

  getByCategory<T>(name: string): Observable<T> {
    return this.http.get<T>(environment.api + this.url_categories + name );
  }
}
