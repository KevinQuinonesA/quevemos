import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Register } from '../models/Register';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class RegisterService {
    
    url:string="https://qvemosbk.herokuapp.com/user/";
  
  
    constructor(private http:HttpClient) { }
  
    authRegister(form:Register):Observable<any>{
      return this.http.post(this.url,form);
    }
  }
  