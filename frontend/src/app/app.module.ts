import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { SearcherComponent } from './shared/components/searcher/searcher.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
//import { CarouselComponent } from './shared/components/carousel/carousel.component';
import { SesionloginComponent } from './pages/sesionlogin/sesionlogin.component';
import { SesionregisterComponent } from './pages/sesionregister/sesionregister.component';
import { MainModule } from './pages/main/main.module';

@NgModule({
  declarations: [
    AppComponent,
   // SearcherComponent,
    //CarouselComponent,
    SesionloginComponent,
    SesionregisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    MainModule
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule { }
