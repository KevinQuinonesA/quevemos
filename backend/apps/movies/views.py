
from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required
import json

import psycopg2
from database.database import db
from sqlalchemy.sql import text
from psycopg2.extras import RealDictCursor
from flask_cors import CORS, cross_origin

appMovie = Blueprint('appMovie', __name__)


@appMovie.route("/movie/get_information/<int:id>", methods=['GET'])
@jwt_required()
@cross_origin()
def get_id(id):
    sql =text( 
           "SELECT * FROM MOVIES WHERE id= :x"
            )
    results =(db.session.execute(sql, {"x":id}))
    results=results.fetchone()
    my_dict = dict(results)
    db.session.commit()

    return  (
       my_dict
    ),200


@appMovie.route("/movie/top/", methods=['GET'])
@jwt_required()
@cross_origin()
def get_top_movies():
   sql =text( 
      " SELECT * "
      "FROM MOVIES WHERE vote_count IS NOT NULL AND vote_average IS NOT NULL "
      "ORDER BY vote_count DESC, vote_average DESC "  
      "FETCH FIRST 10 ROWS ONLY;"
            )
   results =(db.session.execute(sql).cursor)
   colnames = list([desc[0] for desc in results.description])
   result = {
      "data":[]
   }
   for row in results.fetchall():
      row=list(row)
      new = dict(zip(colnames, row))
      result["data"].append(new)
   
   result= dict(result)
   db.session.commit()

   return  (
      (result)
    ),200