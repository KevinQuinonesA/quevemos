#from flask.app import Flask
#from flask_sqlalchemy import SQLAlchemy
#from flask import Flask
from database.database import db
from apps.user.models import User

pelicula_genero = db.Table('pelicula_genero',
    db.Column('movies_id', db.Integer, db.ForeignKey('movies.id'), primary_key=True),
    db.Column('generos_id', db.Integer, db.ForeignKey('generos.id'), primary_key=True)
)
pelicula_idioma = db.Table('pelicula_idioma',
    db.Column('movies_id', db.Integer, db.ForeignKey('movies.id'), primary_key=True),
    db.Column('idiomas_id', db.Integer, db.ForeignKey('idiomas.id'), primary_key=True)
)
pelicula_pais = db.Table('pelicula_pais',
    db.Column('movies_id', db.Integer, db.ForeignKey('movies.id'), primary_key=True),
    db.Column('paises_id', db.Integer, db.ForeignKey('paises.id'), primary_key=True)
)
pelicula_compania = db.Table('pelicula_compania',
    db.Column('movies_id', db.Integer, db.ForeignKey('movies.id'), primary_key=True),
    db.Column('companias_id', db.Integer, db.ForeignKey('companias.id'), primary_key=True)
)
pelicula_lista = db.Table('lista_favoritos',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('movies_id', db.Integer, db.ForeignKey('movies.id'), primary_key=True)
)
class Movie(db.Model):
    # overview --> resumen
    # post path --> cartel
    __tablename__='movies'
    id=db.Column(db.Integer, primary_key=True)
    titulo=db.Column(db.String(100))
    resumen=db.Column(db.Text)
    cartel=db.Column(db.String(256), unique=True, nullable=True)
    es_adultos=db.Column(db.Boolean, default=False, nullable=False)
    recaudacion=db.Column(db.Integer)
    presupuesto=db.Column(db.Integer)
    link=db.Column(db.String(256), nullable=True)
    imdb_id=db.Column(db.String(12), unique=True, nullable=True)
    popularidad=db.Column(db.Integer)
    fecha_estreno=db.Column(db.DateTime)
    duracion=db.Column(db.DateTime)
    calificacion=db.Column(db.Float)
    numero_votaciones=db.Column(db.Integer)
    frase_corta=db.Column(db.String(256))
    ## llaves foraneas
    coleccion_id=db.Column(db.Integer, db.ForeignKey('colections.id'))
    estado_id=db.Column(db.Integer, db.ForeignKey('estados.id'))
    
    '''def __init__(self,titulo,resumen,cartel,es_adultos,recaudacion,presupuesto,link
    ,imdb_id,popularidad,fecha_estreno,duracion,calificacion,numero_votaciones,frase_corta):
        self.titulo = titulo
        self.resumen = resumen
        self.cartel  = cartel
        self.es_adultos  = es_adultos
        self.recaudacion  = recaudacion
        self.presupuesto  = presupuesto
        self.link  = link
        self.imdb_id  = imdb_id
        self.popularidad  = popularidad
        self.fecha_estreno  = fecha_estreno
        self.duracion  = duracion
        self.calificacion  = calificacion
        self.numero_votaciones  = numero_votaciones
        self.frase_corta = frase_corta
    '''
    def __repr__(self):
        return f'{self.titulo}'

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls,id):
        return cls.query.get_or_404(id)

class ListaFav(db.Model):
    __tablename__='listas'
    id=db.Column(db.Integer, primary_key=True)
    ## posible nombre de la lista, ej: pelis de terror, accion, etc
    nombre=db.Column(db.String(30))
    ''' user = session["user"]
    auth = session["auth"]'''
    peliculas=db.relationship('Movie')
    
    def __repr__(self):
        return f'{self.nombre}'

class Coleccion(db.Model):
    __tablename__='colections'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    resumen=db.Column(db.Text)
    peliculas=db.relationship('Movie')
    
    def __repr__(self):
        return f'{self.nombre}'

class Genero(db.Model):
    __tablename__='generos'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    peliculas = db.relationship('Movie', secondary=pelicula_genero,
        backref=db.backref('genero', lazy=True))

    def __repr__(self):
        return f'{self.nombre}'

class Idioma(db.Model):
    __tablename__='idiomas'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    peliculas = db.relationship('Movie', secondary=pelicula_idioma,
        backref=db.backref('idioma', lazy=True))
        
    def __repr__(self):
        return f'{self.nombre}'

class Pais(db.Model):
    __tablename__='paises'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    peliculas = db.relationship('Movie', secondary=pelicula_pais,
        backref=db.backref('pais', lazy=True))
        
    def __repr__(self):
        return f'{self.nombre}'

class Compania(db.Model):
    __tablename__='companias'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    peliculas = db.relationship('Movie', secondary=pelicula_compania,
        backref=db.backref('compania', lazy=True))
        
    def __repr__(self):
        return f'{self.nombre}'

class Estado(db.Model):
    __tablename__='estados'
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(30))
    peliculas=db.relationship('Movie')
    
    def __repr__(self):
        return f'{self.nombre}'
