from flask import Blueprint, request, jsonify
from database.database import db
from apps.user.models import User
from apps.user.models import UserSchema
from werkzeug.security import  check_password_hash
from flask_jwt_extended import get_jwt
from flask_jwt_extended import create_access_token
from datetime import datetime , timedelta,timezone
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from .models import TokenBlocklist
from flask_cors import CORS, cross_origin


appAuth = Blueprint('appAuth', __name__)
ACCESS_EXPIRES = timedelta(hours=1)







@appAuth.route("/login/", methods=['POST'])
@cross_origin()

def login():
    data=request.get_json()
    user = User.query.filter_by(email=data.get('email')).first()
    if not user or not check_password_hash(user.password, data.get("password")):
        return  jsonify(
            {"message":"Credenciales erróneas "}
        ),400
    access_token = create_access_token(identity=str(user.id))
    return {'token': access_token}, 200

@appAuth.route("/logout/", methods=["DELETE"])
@jwt_required()
@cross_origin()
def modify_token():
    jti = get_jwt()["jti"]
    now = datetime.now(timezone.utc)
    db.session.add(TokenBlocklist(jti=jti, created_at=now))
    db.session.commit()
    return jsonify(msg="JWT revoked")


