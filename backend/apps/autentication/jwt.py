
from flask_jwt_extended import JWTManager
jwt=JWTManager()

def init_app(app):
    global  jwt
    jwt = JWTManager(app)
