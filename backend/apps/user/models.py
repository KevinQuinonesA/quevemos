#from flask.app import Flask
#from flask_sqlalchemy import SQLAlchemy
#from flask import Flask
from database.database import db
from marshmallow import Schema,fields
from flask_login import UserMixin


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)

    def __repr__(self):
        return f'<User {self.email}>'

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls,id):
        return cls.query.get_or_404(id)


    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class UserSchema(Schema):
    id=fields.Integer()
    name=fields.String()
    email=fields.String()
    password=fields.String()
