from flask import Blueprint, request, jsonify
from database.database import db
from apps.user.models import User
from apps.user.models import UserSchema
app1 = Blueprint('app1', __name__)
from werkzeug.security import  generate_password_hash
from flask_jwt_extended import jwt_required
from flask_cors import CORS, cross_origin

@app1.route("/user/", methods=['GET'])
@jwt_required()
@cross_origin()
def get_all_users():
    users=User.get_all()
    serializer=UserSchema(many=True)
    data=serializer.dump(users)
    return jsonify(
        data
    ),200

@app1.route("/user/", methods=['POST'])
@cross_origin()
def create_a_user():
    data=request.get_json()
    new_user= User(
        name=data.get('name'),
        email=data.get('email'),
        password= generate_password_hash(data.get('password'), method='sha256')
    )
    new_user.save()
    serializer=UserSchema()
    data=serializer.dump(new_user)
    return jsonify(
        data
    ),201

@app1.route("/user/<int:id>", methods=['GET'])
@jwt_required()
@cross_origin()
def get_user(id):
    user=User.get_by_id(id)
    serializer=UserSchema()
    data=serializer.dump(user)
    return jsonify(
        data
    ),200

@app1.route("/user/<int:id>", methods=['PUT'])
@jwt_required()
@cross_origin()
def update_user(id):
    user=User.get_by_id(id)
    data=request.get_json() 
    user.name=data.get('name')
    user.email=data.get('email')
    user.password=data.get('password')
    db.session.commit()
    serializer=UserSchema()
    data=serializer.dump(user)
    return jsonify(
        data
    ),200

@app1.route("/user/<int:id>", methods=['DELETE'])
@jwt_required()
@cross_origin()
def delete_user(id):
    user=User.get_by_id(id)
    user.delete()
    return  jsonify(
        {"message":"Usuario Eliminado"}
    ),204
