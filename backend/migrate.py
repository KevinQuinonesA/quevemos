import psycopg2
import csv


conn = psycopg2.connect(database="d8nrkk3i7ed400",
						user='ynoawvxszsvxwv', password='c7d8da948bf0b0e64c774da30fa67143a5567b12882870416e80b3535cfc9fa7',
						host='ec2-34-193-112-164.compute-1.amazonaws.com', port='5432'
)

conn.autocommit = True
cursor = conn.cursor()      


sql = '''CREATE TABLE MOVIES(adult varchar(500),\
    belongs_to_collection varchar(1000),\
    budget int,\
    genres varchar(1000),\
    homepage  varchar(300),\
    id   int NOT NULL,\
    imdb_id  varchar(100),\
    original_language varchar(10),\
    original_title varchar(350),\
    overview varchar(1000),\
    popularity varchar(20), \
    poster_path varchar(250),\
    production_companies varchar(2000),\
    production_countries  varchar(2000),\
    release_date varchar(20),\
    revenue numeric, \
    runtime numeric, \
    spoken_languages varchar(1000),\
    status varchar(30),\
    tagline varchar(300),\
    title varchar(350),\
    video varchar(20),\
    vote_average numeric ,\
    vote_count int

);'''


cursor.execute(sql)



copy_sql = """
           COPY movies FROM stdin WITH CSV HEADER
           DELIMITER as ','
           """
with open('static/media/movies_metadata.csv', 'r', encoding="utf8") as f:
    cursor.copy_expert(sql=copy_sql, file=f)
    conn.commit()
    cursor.close()

