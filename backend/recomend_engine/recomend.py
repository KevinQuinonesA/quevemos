import json
import pandas as pd
import numpy as np
import seaborn as sns
import math, nltk, warnings
from nltk.corpus import wordnet
from sklearn import linear_model
from sklearn.neighbors import NearestNeighbors
from fuzzywuzzy import fuzz

PS = nltk.stem.PorterStemmer()

def load_tmdb_movies(path):
    df = pd.read_csv(path,engine="python")
    df['title_year'] = pd.to_datetime(df['title_year'], format='%Y')
    return df

def load_tmdb_credits(path):
    df = pd.read_csv(path)
    json_columns = ['cast', 'crew']
    for column in json_columns:
        df[column] = df[column].apply(json.loads)
    return df
LOST_COLUMNS = [
    'actor_1_facebook_likes',
    'actor_2_facebook_likes',
    'actor_3_facebook_likes',
    'aspect_ratio',
    'cast_total_facebook_likes',
    'color',
    'content_rating',
    'director_facebook_likes',
    'facenumber_in_poster',
    'movie_facebook_likes',
    'movie_imdb_link',
    'num_critic_for_reviews'
    ]
TMDB_TO_IMDB_SIMPLE_EQUIVALENCIES = {
    'budget': 'budget',
    'genres': 'genres',
    'revenue': 'gross',
    'title': 'movie_title',
    'runtime': 'duration',
    'original_language': 'language',
    'keywords': 'plot_keywords',
    'vote_count': 'num_voted_users'}
IMDB_COLUMNS_TO_REMAP = {}
def safe_access(container, index_values):
    result = container
    try:
        for idx in index_values:
            result = result[idx]
        return result
    except IndexError or KeyError:
        return pd.np.nan
def get_director(crew_data):
    directors = [x['name'] for x in crew_data if x['job'] == 'Director']
    return safe_access(directors, [0])
def pipe_flatten_names(keywords):
    return '|'.join([x['name'] for x in keywords])
def convert_to_original_format(movies, credits):
    tmdb_movies = movies.copy()
    tmdb_movies.rename(columns=TMDB_TO_IMDB_SIMPLE_EQUIVALENCIES, inplace=True)
    tmdb_movies['title_year'] = pd.to_datetime(tmdb_movies['title_year']).apply(lambda x: x.year)
    tmdb_movies['director_name'] = credits['crew'].apply(get_director)
    tmdb_movies['actor_1_name'] = credits['cast'].apply(lambda x: safe_access(x, [1, 'name']))
    tmdb_movies['actor_2_name'] = credits['cast'].apply(lambda x: safe_access(x, [2, 'name']))
    tmdb_movies['actor_3_name'] = credits['cast'].apply(lambda x: safe_access(x, [3, 'name']))
    return tmdb_movies

def count_word(df, ref_col, liste):
    keyword_count = dict()
    for s in liste: keyword_count[s] = 0
    for liste_keywords in df[ref_col].str.split('|'):        
        if type(liste_keywords) == float and pd.isnull(liste_keywords): continue        
        for s in [s for s in liste_keywords if s in liste]: 
            if pd.notnull(s): keyword_count[s] += 1
    keyword_occurences = []
    for k,v in keyword_count.items():
        keyword_occurences.append([k,v])
    keyword_occurences.sort(key = lambda x:x[1], reverse = True)
    return keyword_occurences, keyword_count

def get_stats(gr):
    return {'min':gr.min(),'max':gr.max(),'count': gr.count(),'mean':gr.mean()}

def keywords_inventory(dataframe, colonne = 'plot_keywords'):
    PS = nltk.stem.PorterStemmer()
    keywords_roots  = dict()  
    keywords_select = dict()  
    category_keys = []
    icount = 0
    for s in dataframe[colonne]:
        if pd.isnull(s): continue
        for t in s.split('|'):
            t = t.lower() ; racine = PS.stem(t)
            if racine in keywords_roots:                
                keywords_roots[racine].add(t)
            else:
                keywords_roots[racine] = {t}
    
    for s in keywords_roots.keys():
        if len(keywords_roots[s]) > 1:  
            min_length = 1000
            for k in keywords_roots[s]:
                if len(k) < min_length:
                    clef = k ; min_length = len(k)            
            category_keys.append(clef)
            keywords_select[s] = clef
        else:
            category_keys.append(list(keywords_roots[s])[0])
            keywords_select[s] = list(keywords_roots[s])[0]
    return category_keys, keywords_roots, keywords_select

def remplacement_df_keywords(df, dico_remplacement, roots = False):
    df_new = df.copy(deep = True)
    for index, row in df_new.iterrows():
        chaine = row['plot_keywords']
        if pd.isnull(chaine): continue
        nouvelle_liste = []
        for s in chaine.split('|'): 
            clef = PS.stem(s) if roots else s
            if clef in dico_remplacement.keys():
                nouvelle_liste.append(dico_remplacement[clef])
            else:
                nouvelle_liste.append(s)       
        df_new.at[index, 'plot_keywords']= '|'.join(nouvelle_liste)
    return df_new

def get_synonymes(mot_cle):
    lemma = set()
    for ss in wordnet.synsets(mot_cle):
        for w in ss.lemma_names():
            index = ss.name().find('.')+1
            if ss.name()[index] == 'n': lemma.add(w.lower().replace('_',' '))
    return lemma   

def test_keyword(mot, key_count, threshold):
    return (False , True)[key_count.get(mot, 0) >= threshold]

def remplacement_df_low_frequency_keywords(df, keyword_occurences):
    df_new = df.copy(deep = True)
    key_count = dict()
    for s in keyword_occurences: 
        key_count[s[0]] = s[1]    
    for index, row in df_new.iterrows():
        chaine = row['plot_keywords']
        if pd.isnull(chaine): continue
        nouvelle_liste = []
        for s in chaine.split('|'): 
            if key_count.get(s, 4) > 3: nouvelle_liste.append(s)
        df_new.at[index, 'plot_keywords']= '|'.join(nouvelle_liste)
    return df_new

def fill_year(df):
    col = ['director_name', 'actor_1_name', 'actor_2_name', 'actor_3_name']
    usual_year = [0 for _ in range(4)]
    var        = [0 for _ in range(4)]
    for i in range(4):
        usual_year[i] = df.groupby(col[i])['title_year'].mean()
    actor_year = dict()
    for i in range(4):
        for s in usual_year[i].index:
            if s in actor_year.keys():
                if pd.notnull(usual_year[i][s]) and pd.notnull(actor_year[s]):
                    actor_year[s] = (actor_year[s] + usual_year[i][s])/2
                elif pd.isnull(actor_year[s]):
                    actor_year[s] = usual_year[i][s]
            else:
                actor_year[s] = usual_year[i][s]
        
    missing_year_info = df[df['title_year'].isnull()]
    icount_replaced = 0
    for index, row in missing_year_info.iterrows():
        value = [ np.NaN for _ in range(4)]
        icount = 0 ; sum_year = 0
        for i in range(4):            
            var[i] = df.loc[index][col[i]]
            if pd.notnull(var[i]): value[i] = actor_year[var[i]]
            if pd.notnull(value[i]): icount += 1 ; sum_year += actor_year[var[i]]
        if icount != 0: sum_year = sum_year / icount 

        if int(sum_year) > 0:
            icount_replaced += 1
            df.at[index, 'title_year']=int(sum_year)
    return 

def variable_linreg_imputation(df, col_to_predict, ref_col):
    regr = linear_model.LinearRegression()
    test = df[[col_to_predict,ref_col]].dropna(how='any', axis = 0)
    X = np.array(test[ref_col])
    Y = np.array(test[col_to_predict])
    X = X.reshape(len(X),1)
    Y = Y.reshape(len(Y),1)
    regr.fit(X, Y)
    test = df[df[col_to_predict].isnull() & df[ref_col].notnull()]
    for index, row in test.iterrows():
        value = float(regr.predict([[row[ref_col]]]))
        df.at[index, col_to_predict]= value

def entry_variables(df, id_entry): 
    col_labels = []    
    if pd.notnull(df['director_name'].iloc[id_entry]):
        for s in df['director_name'].iloc[id_entry].split('|'):
            col_labels.append(s)
            
    for i in range(3):
        column = 'actor_NUM_name'.replace('NUM', str(i+1))
        if pd.notnull(df[column].iloc[id_entry]):
            for s in df[column].iloc[id_entry].split('|'):
                col_labels.append(s)
                
    if pd.notnull(df['plot_keywords'].iloc[id_entry]):
        for s in df['plot_keywords'].iloc[id_entry].split('|'):
            col_labels.append(s)
    return col_labels

def add_variables(df, REF_VAR):    
    for s in REF_VAR: df[s] = pd.Series([0 for _ in range(len(df))])
    colonnes = ['genres', 'actor_1_name', 'actor_2_name',
                'actor_3_name', 'director_name', 'plot_keywords']
    for categorie in colonnes:
        for index, row in df.iterrows():
            if pd.isnull(row[categorie]): continue
            for s in row[categorie].split('|'):
                if s in REF_VAR: df.at[index, s]=1
    return df

def recommand(df, id_entry):    
    df_copy = df.copy(deep = True)    
    liste_genres = set()
    for s in df['genres'].str.split('|').values:
        liste_genres = liste_genres.union(set(s))    
    variables = entry_variables(df_copy, id_entry)
    variables += list(liste_genres)
    df_new = add_variables(df_copy, variables)
    X = df_new[variables].values
    nbrs = NearestNeighbors(n_neighbors=31, algorithm='auto', metric='euclidean').fit(X)

    distances, indices = nbrs.kneighbors(X)    
    xtest = df_new.iloc[id_entry][variables].values
    xtest = xtest.reshape(1, -1)

    distances, indices = nbrs.kneighbors(xtest)

    return indices[0][:]
    
def extract_parameters(df, liste_films):     
    parametres_films = ['_' for _ in range(31)]
    i = 0
    max_users = -1
    for index in liste_films:
        aux_list=['movie_title', 'title_year','imdb_score', 'num_user_for_reviews','num_voted_users']
        parametres_films[i] = list(df[aux_list].iloc[index])  
        parametres_films[i].append(index)
        max_users = max(max_users, parametres_films[i][4] )
        i += 1    
    title_main = parametres_films[0][0]
    annee_ref  = parametres_films[0][1]
    parametres_films.sort(key = lambda x:critere_selection(title_main, max_users,
                                    annee_ref, x[0], x[1], x[2], x[4]), reverse = True)

    return parametres_films 

def sequel(titre_1, titre_2):    
    if fuzz.ratio(titre_1, titre_2) > 50 or fuzz.token_set_ratio(titre_1, titre_2) > 50:
        return True
    else:
        return False

def critere_selection(title_main, max_users, annee_ref, titre, annee, imdb_score, votes):    
    gaussian_filter = lambda x,y,sigma: math.exp(-(x-y)**2/(2*sigma**2))
    if pd.notnull(annee_ref):
        facteur_1 = gaussian_filter(annee_ref, annee, 20)
    else:
        facteur_1 = 1        

    sigma = max_users * 1.0

    if pd.notnull(votes):
        facteur_2 = gaussian_filter(votes, max_users, sigma)
    else:
        facteur_2 = 0
        
    if sequel(title_main, titre):
        note = 0
    else:
        note = imdb_score**2 * facteur_1 * facteur_2
    
    return note

def add_to_selection(film_selection, parametres_films):    
    film_list = film_selection[:]
    icount = len(film_list)    
    for i in range(31):
        already_in_list = False
        for s in film_selection:
            if s[0] == parametres_films[i][0]: already_in_list = True
            if sequel(parametres_films[i][0], s[0]): already_in_list = True
            for item in film_list:
              if(item[0]==parametres_films[i][0]):
                already_in_list = True
                break          
        if already_in_list: continue
        icount += 1
        if icount <= 5:
            film_list.append(parametres_films[i])
    return film_list

def remove_sequels(film_selection):    
    removed_from_selection = []
    for i, film_1 in enumerate(film_selection):
        for j, film_2 in enumerate(film_selection):
            if j <= i: continue 
            if sequel(film_1[0], film_2[0]): 
                last_film = film_2[0] if film_1[1] < film_2[1] else film_1[0]
                removed_from_selection.append(last_film)

    film_list = [film for film in film_selection if film[0] not in removed_from_selection]

    return film_list   

def find_similarities(df, id_entry, del_sequels = True, verbose = False):    
    if verbose: 
        print(90*'_' + '\n' + "QUERY: films similar to id={} -> '{}'".format(id_entry,
                                df.iloc[id_entry]['movie_title']))
    liste_films = recommand(df, id_entry)
    parametres_films = extract_parameters(df, liste_films)
    film_selection = []
    film_selection = add_to_selection(film_selection, parametres_films)
    if del_sequels: film_selection = remove_sequels(film_selection)
    film_selection = add_to_selection(film_selection, parametres_films)
    selection_titres = []
    for i,s in enumerate(film_selection):
        selection_titres.append([s[0].replace(u'\xa0', u''), s[5]])
        if verbose: print("nº{:<2}     -> {:<30}".format(i+1, s[0]))

    return selection_titres

def initial_run():
    pd.options.display.max_columns = 50
    warnings.filterwarnings('ignore')
    credits = load_tmdb_credits("recomend_engine/files_needed/tmdb_5000_credits.csv")
    movies = load_tmdb_movies("recomend_engine/files_needed/tmdb_5000_movies.csv")
    df_initial = convert_to_original_format(movies, credits)

    tab_info=pd.DataFrame(df_initial.dtypes).T.rename(index={0:'column type'})
    tab_info=tab_info.append(pd.DataFrame(df_initial.isnull().sum()).T.rename(index={0:'null values'}))
    tab_info=tab_info.append(pd.DataFrame(df_initial.isnull().sum()/df_initial.shape[0]*100).T.
                            rename(index={0:'null values (%)'}))

    set_keywords = set()
    for liste_keywords in df_initial['plot_keywords'].str.split('|').values:
        if isinstance(liste_keywords, float): continue
        set_keywords = set_keywords.union(liste_keywords)
        
    keyword_occurences, dum = count_word(df_initial, 'plot_keywords', set_keywords)
    missing_df = df_initial.isnull().sum(axis=0).reset_index()
    missing_df.columns = ['column_name', 'missing_count']
    missing_df['filling_factor'] = (df_initial.shape[0] 
                                    - missing_df['missing_count']) / df_initial.shape[0] * 100
    missing_df.sort_values('filling_factor').reset_index(drop = True)

    df_initial['decade'] = df_initial['title_year'].apply(lambda x:((x-1900)//10)*10)
    test = df_initial['title_year'].groupby(df_initial['decade']).apply(get_stats).unstack()

    genre_labels = set()
    for s in df_initial['genres'].str.split('|').values:
        genre_labels = genre_labels.union(set(s))

    keyword_occurences, dum = count_word(df_initial, 'genres', genre_labels)
    keyword_occurences[:5]

    df_duplicate_cleaned = df_initial

    keywords, keywords_roots, keywords_select = keywords_inventory(df_duplicate_cleaned,colonne = 'plot_keywords')
    df_keywords_cleaned = remplacement_df_keywords(df_duplicate_cleaned, keywords_select,roots = True)
    keyword_occurences, keywords_count = count_word(df_keywords_cleaned,'plot_keywords',keywords)
    nltk.download('wordnet')
    
    keyword_occurences.sort(key = lambda x:x[1], reverse = False)
    key_count = dict()
    for s in keyword_occurences:
        key_count[s[0]] = s[1]
    remplacement_mot = dict()
    icount = 0

    for key, value in remplacement_mot.items():
        if value in remplacement_mot.keys():
            remplacement_mot[key] = remplacement_mot[value]         

    df_keywords_synonyms = \
                remplacement_df_keywords(df_keywords_cleaned, remplacement_mot, roots = False)   
    keywords, keywords_roots, keywords_select = \
                keywords_inventory(df_keywords_synonyms, colonne = 'plot_keywords')

    new_keyword_occurences, keywords_count = count_word(df_keywords_synonyms,'plot_keywords',keywords)

    df_keywords_occurence = \
        remplacement_df_low_frequency_keywords(df_keywords_synonyms, new_keyword_occurences)
    keywords, keywords_roots, keywords_select = \
        keywords_inventory(df_keywords_occurence, colonne = 'plot_keywords') 
    df_keywords_occurence   

    keywords.remove('')
    new_keyword_occurences, keywords_count = count_word(df_keywords_occurence,'plot_keywords',keywords)

    LOST_COLUMNS = [
        'actor_1_facebook_likes',
        'actor_2_facebook_likes',
        'actor_3_facebook_likes',
        'aspect_ratio',
        'cast_total_facebook_likes',
        'color',
        'content_rating',
        'director_facebook_likes',
        'facenumber_in_poster',
        'movie_facebook_likes',
        'movie_imdb_link',
        'num_critic_for_reviews',
                    ]

    new_col_order = ['movie_title', 'title_year', 'genres', 'plot_keywords', 
                    'director_name', 'actor_1_name', 'actor_2_name', 'actor_3_name',
                    'director_facebook_likes', 'actor_1_facebook_likes', 'actor_2_facebook_likes',
                    'actor_3_facebook_likes', 'movie_facebook_likes', 'num_critic_for_reviews', 
                    'num_user_for_reviews','num_voted_users', 'language', 'country',
                    'imdb_score','movie_imdb_link', 'color', 'duration', 'gross', ]
    new_col_order = [col for col in new_col_order if col not in LOST_COLUMNS]
    new_col_order
    new_col_order = [IMDB_COLUMNS_TO_REMAP[col] if col in IMDB_COLUMNS_TO_REMAP else col
                    for col in new_col_order]
    new_col_order = [TMDB_TO_IMDB_SIMPLE_EQUIVALENCIES[col] if col in TMDB_TO_IMDB_SIMPLE_EQUIVALENCIES else col
                    for col in new_col_order]
    df_var_cleaned = df_keywords_occurence[new_col_order]

    missing_df = df_var_cleaned.isnull().sum(axis=0).reset_index()
    missing_df.columns = ['column_name', 'missing_count']
    missing_df['filling_factor'] = (df_var_cleaned.shape[0] 
                                    - missing_df['missing_count']) / df_var_cleaned.shape[0] * 100

    missing_df = df_var_cleaned.isnull().sum(axis=0).reset_index()
    missing_df.columns = ['column_name', 'missing_count']
    missing_df['filling_factor'] = (df_var_cleaned.shape[0] 
                                    - missing_df['missing_count']) / df_var_cleaned.shape[0] * 100
    missing_df = missing_df.sort_values('filling_factor').reset_index(drop = True)

    df_filling = df_var_cleaned.copy(deep=True)
    missing_year_info = df_filling[df_filling['title_year'].isnull()][['director_name','actor_1_name', 'actor_2_name', 'actor_3_name']]

    fill_year(df_filling)

    icount = 0
    for index, row in df_filling[df_filling['plot_keywords'].isnull()].iterrows():
        icount += 1
        liste_mot = row['movie_title'].strip().split()
        new_keyword = []
        for s in liste_mot:
            lemma = get_synonymes(s)
            for t in list(lemma):
                if t in keywords: 
                    new_keyword.append(t) 
        if new_keyword:
            df_filling.at[index, 'plot_keywords'] = '|'.join(new_keyword)
    variable_linreg_imputation(df_filling, 'gross', 'num_voted_users')

    df = df_filling.copy(deep = True)
    missing_df = df.isnull().sum(axis=0).reset_index()
    missing_df.columns = ['column_name', 'missing_count']
    missing_df['filling_factor'] = (df.shape[0] 
                                    - missing_df['missing_count']) / df.shape[0] * 100
    missing_df = missing_df.sort_values('filling_factor').reset_index(drop = True)

    df = df_filling.copy(deep=True)
    df.reset_index(inplace = True, drop = True)

    return df

# df=initial_run()
# dum = find_similarities(df, 12, del_sequels = True, verbose = True)
# print(dum)
# df.to_pickle("./recomend_engine/files_needed/df.pkl")
# import os
# cwd = os.getcwd()
# print(cwd)