import pandas as pd
from recomend_engine.recomend import find_similarities
from flask import Blueprint, request, jsonify
import json
from flask_cors import CORS, cross_origin
from sqlalchemy.sql import text
from database.database import db

ia_routes = Blueprint('ia_routes', __name__)

#@ia_routes.route("/recomend/<int:id>", methods=['GET'])
def get_recommend(id):
    df = pd.read_pickle("./recomend_engine/files_needed/df.pkl")
    recomendation = find_similarities(df, id, del_sequels = True, verbose = True)
    # ids = {}
    # i=0
    # for item in recomendation:
    #     ids[i]=str(item[1])
    #     i+=1
    return recomendation


@ia_routes.route("/recomend/<int:id>", methods=['GET'])
@cross_origin()
def get_id(id):
    recomend=get_recommend(id)
    my_recomend_dict={}
    index=0
    for id in recomend:
        id=str(id[1])
        sql =text( 
            "SELECT * FROM MOVIES_COMPLETE WHERE id= :x"
                )
        results =(db.session.execute(sql, {"x":id}))
        results=results.fetchone()
        my_dict = dict(results)
        my_recomend_dict[str(index)]=my_dict
        index+=1

    db.session.commit()

    return  (
       my_recomend_dict
    ),200