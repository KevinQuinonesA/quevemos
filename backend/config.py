import os
from apps.autentication.views import ACCESS_EXPIRES
from decouple import config

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = '57e19ea558d4967a552d03deece34a70'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL').replace("postgres://", "postgresql://", 1)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    ENV="development"
    DEVELOPMENT=True
    DEBUG=True
    JWT_ACCESS_TOKEN_EXPIRES= ACCESS_EXPIRES
    SQLALCHEMY_DATABASE_URI= "postgresql://ynoawvxszsvxwv:c7d8da948bf0b0e64c774da30fa67143a5567b12882870416e80b3535cfc9fa7@ec2-34-193-112-164.compute-1.amazonaws.com:5432/d8nrkk3i7ed400"
    SQLALCHEMY_TRACK_MODIFICATIONS = False