import psycopg2
import csv


conn = psycopg2.connect(database="d8nrkk3i7ed400",
						user='ynoawvxszsvxwv', password='c7d8da948bf0b0e64c774da30fa67143a5567b12882870416e80b3535cfc9fa7',
						host='ec2-34-193-112-164.compute-1.amazonaws.com', port='5432'
)

conn.autocommit = True
cursor = conn.cursor()      


sql = '''CREATE TABLE MOVIES_COMPLETE(id int NOT NULL,
    movie_title varchar(350),\
    title_year numeric,\
    genres varchar(350),\
    plot_keywords varchar(500),\
    director_name  varchar(500),\
    actor_1_name   varchar(500),\
    actor_2_name  varchar(500),\
    actor_3_name varchar(500),\
    num_user_for_reviews numeric,\
    num_voted_users numeric,\
    language varchar(100), \
    country varchar(50),\
    imdb_score numeric,\
    duration  numeric,\
    gross numeric
);'''


cursor.execute(sql)



copy_sql = """
           COPY movies_complete FROM stdin WITH CSV HEADER
           DELIMITER as ','
           """
with open('static/media/movies_metadata2.csv', 'r', encoding="utf8") as f:
    cursor.copy_expert(sql=copy_sql, file=f)
    conn.commit()
    cursor.close()

