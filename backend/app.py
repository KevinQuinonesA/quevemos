from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from database import database
from apps.user.views import app1
from apps.user.models import User
from flask_jwt_extended import JWTManager
from apps.autentication.views import appAuth
from apps.autentication import jwt
from apps.autentication.models import TokenBlocklist
from database.database import db
from apps.movies.views import appMovie
from recomend_engine.ia_routes import ia_routes
from flask_cors import CORS

from recomend_engine.recomend import initial_run
import os

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
# setup with the configuration provided
app.config.from_object('config.DevelopmentConfig')
    
# setup all our dependencies
database.init_app(app)
jwt = JWTManager(app)
    
@jwt.token_in_blocklist_loader
def check_if_token_revoked(jwt_header, jwt_payload):
    jti = jwt_payload["jti"]
    token = db.session.query(TokenBlocklist.id).filter_by(jti=jti).scalar()
    return token is not None
            
# register blueprint
app.register_blueprint(app1)
app.register_blueprint(appAuth)
app.register_blueprint(appMovie)
app.register_blueprint(ia_routes)
#app.register_blueprint(app2, url_prefix="/app2")
    
    #Preprocesing DataFrame to IA Component
if(not os.path.isfile("./recomend_engine/files_needed/df.pkl")):
    print("Building DataFrame ...")
    df=initial_run()
    df.to_pickle("./recomend_engine/files_needed/df.pkl")

if __name__=='__main__':
    app.run()